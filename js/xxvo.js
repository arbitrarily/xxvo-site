$(function() {

	var w = $(window);
	var c = $(".container");
	var navStatic = $(".navbar-nav");
	var navFixed = $(".navbar-nav.fixed");

	// Size Stick Nav
	w.resize(function(){
		if (navFixed.length > 0 ) {
      navFixed.width(c.outerWidth());
    } else {
    	navStatic.width(c.outerWidth()); // fix if people trigger window resize & scroll
    }
	}).resize();

	// Sticky Nav
  w.bind("scroll", function() {
    if (w.scrollTop() > 280) {
      navStatic.addClass("fixed");
      navFixed.width(c.outerWidth());
      $(".scrollTop.hidden, .smallLogo.hidden").removeClass("hidden");
    } else {
      $(".navbar-nav.fixed").removeClass("fixed");
      $(".scrollTop, .smallLogo").addClass("hidden");
    }
  });

  // Scroll To Top
  $(".scrollTop").click(function() {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	  return false;
	});

});